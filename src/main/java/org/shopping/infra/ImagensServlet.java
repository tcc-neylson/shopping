package org.shopping.infra;

import java.io.*;
import java.util.UUID;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.shopping.infra.storage.FileStorage;

/**
 * Servlet implementation class ImagesServlet
 */
public class ImagensServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Inject
	private FileStorage storage;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ImagensServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String id = request.getParameter("aplicacaoId") + "";

		File imagem = storage.obterArquivo(id, "imagem");
		InputStream stream = new FileInputStream(imagem);
		OutputStream out = response.getOutputStream();

		if (stream != null) {
			IOUtils.copy(stream, out);
			stream.close();
		}

		out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
	}

}
