package org.shopping.infra;


import javax.annotation.Resource;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class GerenciadorDeBancoDeDados {


    @Resource(mappedName = "java:jboss/datasources/ShoppingDS")
    private DataSource dataSource;

    public String runSql(String sql) {
        try {
            Connection conexao = dataSource.getConnection();

            Statement stmt = conexao.createStatement();
            boolean resultado = stmt.execute(sql);

            return "resultado: " + resultado;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return "";
    }

    public boolean criarBancoDeDados(String nome) {
        try {
            Connection conexao = dataSource.getConnection();

            PreparedStatement stmt = conexao.prepareStatement("CREATE DATABASE IF NOT EXISTS " + nome + " CHARACTER SET utf8 COLLATE utf8_general_ci");

            return stmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    public boolean excluirBancoDeDados(String nome) {
        try {
            Connection conexao = dataSource.getConnection();

            PreparedStatement stmt = conexao.prepareStatement("DROP DATABASE IF EXISTS " + nome);

            return stmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }
}
