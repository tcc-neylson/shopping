package org.shopping.infra.storage;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;

import javax.enterprise.context.ApplicationScoped;

import org.apache.commons.io.FileUtils;

@ApplicationScoped
public class LocalFileStorage implements FileStorage {

	private String rootPath = FileUtils.getUserDirectoryPath() + "/shopping/uploads";

	public File salvarArquivo(InputStream inputStream, String pasta,
			String nomeDoArquivo) throws IOException {

		File file = Paths.get(rootPath, pasta, nomeDoArquivo).toFile();
		FileUtils.copyInputStreamToFile(inputStream, file);
		
		return file;
	}

	@Override
	public void removerPasta(String pasta) throws IOException {
		FileUtils.deleteDirectory(Paths.get(rootPath, pasta).toFile());
	}

	public boolean arquivoExiste(String pasta, String nomeDoArquivo) {
		File file = Paths.get(rootPath, pasta, nomeDoArquivo).toFile();
	
		return file.exists();
	}
	
	public File obterArquivo(String pasta, String nomeDoArquivo) {
		return Paths.get(rootPath, pasta, nomeDoArquivo).toFile();
	}
	
	public void removerArquivo(String pasta, String nomeDoArquivo) {
		File file = Paths.get(rootPath, pasta, nomeDoArquivo).toFile();
		
		FileUtils.deleteQuietly(file);
	}
}
