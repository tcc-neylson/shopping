package org.shopping.infra;

import org.apache.commons.io.FileUtils;
import org.shopping.entidades.jpa.Aplicacao;
import org.shopping.infra.storage.FileStorage;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;


/**
 * Created by neylson on 26/03/16.
 */
public class ImplantadorDeAplicacoes {

    @Inject
    private FileStorage storage;


    public void deploy(Aplicacao aplicacao) throws IOException {

        File deployments = Paths.get("../standalone/deployments")
                .toAbsolutePath().normalize().toFile();
        FileUtils.copyFileToDirectory(this.getArquivoWar(aplicacao), deployments);
    }

    public void undeploy(Aplicacao aplicacao) {

        File file = Paths.get("../standalone/deployments", aplicacao.getContexto() + ".war")
                .toAbsolutePath().normalize().toFile();
        FileUtils.deleteQuietly(file);
    }

    private File getArquivoWar(Aplicacao aplicacao) {
        String pasta = aplicacao.getId().toString();
        String nomeDoArquivoWar = aplicacao.getContexto() + ".war";

        return storage.obterArquivo(pasta, nomeDoArquivoWar);
    }
}
