package org.shopping.infra;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

@ApplicationScoped
public class MessagesHelper {

	@Inject
	private FacesContext facesContext;
	
	
	public void addMessage(FacesMessage facesMessage) {
		facesContext.addMessage(null, facesMessage);
	}
}
