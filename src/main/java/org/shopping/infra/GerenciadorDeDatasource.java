package org.shopping.infra;


import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class GerenciadorDeDatasource {

    private String hostMysql = "172.17.0.2";
    private String usuarioMysql = "root";
    private String senhaMysql = "123456";

    /**
     * Cria um novo datasource no wildfly, realizando uma requisição http.
     *
     * @param nomeDoDatasource Nome do datasource
     * @return bool
     */
    public boolean criar(String nomeDoDatasource, String nomeDoBancoDeDados)
    {
        String conteudoDaRequisicao = this.criarJson(nomeDoDatasource, nomeDoBancoDeDados);

        HttpHost httpHost = new HttpHost("localhost", 9990, "http");
        HttpClientContext context = HttpClientContext.create();

        HttpPost httpPost = new HttpPost("/management");
        httpPost.setHeader("Content-type", "application/json");
        try {
            httpPost.setEntity(new StringEntity(conteudoDaRequisicao));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return false;
        }

        CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(AuthScope.ANY,  new UsernamePasswordCredentials("admin", "123456"));
        context.setCredentialsProvider(credentialsProvider);

        HttpClient client = HttpClients.createDefault();
        HttpResponse response;
        try {
             response = client.execute(httpHost, httpPost, context);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return response.getStatusLine().getStatusCode() == 200;
    }

    private String criarJson(String nomeDoDatasource, String nomeDoBancoDeDados) {

        return "{" +
                    "\"operation\":\"add\"," +
                    "\"address\":[{\"subsystem\": \"datasources\"}, {\"data-source\": \"" + nomeDoDatasource +"\"}]," +
                    "\"connection-url\": \"jdbc:mysql://" + hostMysql + ":3306/" + nomeDoBancoDeDados + "\"," +
                    "\"driver-name\": \"mysql\"," +
                    "\"jndi-name\": \"java:jboss/datasources/" + nomeDoDatasource+ "\"," +
                    "\"user-name\": \"" + usuarioMysql + "\"," +
                    "\"password\": \"" + senhaMysql + "\"" +
                "}";
    }

}
