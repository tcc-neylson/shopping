package org.shopping.managedbeans;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.shopping.infra.GerenciadorDeBancoDeDados;

import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;

@Named
public class TerminalBean {


    @Inject
    private transient FacesContext context;

    @Inject
    private GerenciadorDeBancoDeDados gerenciadorDeBancoDeDados;

    public String handleCommand(String command, String[] params) {

        if(command.equals("sql")) {

            if(params.length > 0) {
                return gerenciadorDeBancoDeDados.runSql(StringUtils.join(params, " "));
            }

        } else if(command.equals("a")) {
            String contexto = "appapp";

            // realiza uma requisição http
            // biblioteca usada: https://hc.apache.org/httpcomponents-client-4.5.x/

            CloseableHttpClient client = HttpClients.createDefault();
            HttpHost targetHost = new HttpHost("localhost", 9990, "http");
            HttpClientContext context = HttpClientContext.create();
            HttpPost httppost = new HttpPost("/management");
            httppost.setHeader("Content-type", "application/json");

            // parte necessária para autenticação
            CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
            credentialsProvider.setCredentials(AuthScope.ANY,  new UsernamePasswordCredentials("admin", "123456"));
            context.setCredentialsProvider(credentialsProvider);

            try {
                // conteúdo da requisição em json
                httppost.setEntity(new StringEntity(
                    "{\"operation\":\"add\"," +
                    "\"address\":[{\"subsystem\": \"datasources\"}, {\"data-source\": \"" + contexto +"DS\"}]," +
                    "\"connection-url\": \"jdbc:mysql://172.17.0.2:3306/" + contexto + "\"," +
                    "\"driver-name\": \"mysql\"," +
                    "\"jndi-name\": \"java:jboss/datasources/" + contexto + "DS\"}"
                ));
                HttpResponse response = client.execute(targetHost, httppost, context);
                HttpEntity entity = response.getEntity();

                return EntityUtils.toString(entity);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return "";

        } else if(command.equals("e")) {
            String contexto = "appapp";
            String corpoDaRequisicao =
                "{\"operation\":\"add\"," +
                "\"address\":[{\"subsystem\": \"datasources\"}, {\"data-source\": \"" + contexto +"DS\"}]," +
                "\"connection-url\": \"jdbc:mysql://172.17.0.2:3306/" + contexto + "\"," +
                "\"driver-name\": \"mysql\"," +
                "\"jndi-name\": \"java:jboss/datasources/" + contexto + "DS\"}";


            try {
                HttpHost httpHost = new HttpHost("localhost", 9990, "http");
                HttpClientContext context = HttpClientContext.create();

                HttpPost httpPost = new HttpPost("/management");
                httpPost.setHeader("Content-type", "application/json");
                httpPost.setEntity(new StringEntity(corpoDaRequisicao));

                CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
                credentialsProvider.setCredentials(AuthScope.ANY,  new UsernamePasswordCredentials("admin", "123456"));
                context.setCredentialsProvider(credentialsProvider);

                HttpClient client = HttpClients.createDefault();
                HttpResponse response = client.execute(httpHost, httpPost, context);

                return EntityUtils.toString(response.getEntity());
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        return command + " not found";
    }
}
