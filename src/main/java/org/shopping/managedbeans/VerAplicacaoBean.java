package org.shopping.managedbeans;


import org.shopping.entidades.jpa.Aplicacao;
import org.shopping.services.AplicacaoService;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

@Named
public class VerAplicacaoBean {

    @Inject
    private AplicacaoService aplicacaoService;

    @Inject
    private HttpServletRequest request;

    private Aplicacao aplicacao;

    @PostConstruct
    public void carregar() {
        Long aplicacaoId = Long.valueOf(request.getParameter("id"));
        aplicacao = aplicacaoService.buscar(aplicacaoId);
    }

    public Aplicacao getAplicacao() {
        return aplicacao;
    }



}
