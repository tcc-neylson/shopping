package org.shopping.managedbeans;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import org.apache.commons.codec.digest.DigestUtils;
import org.primefaces.event.FileUploadEvent;
import org.shopping.entidades.jpa.Aplicacao;
import org.shopping.infra.GerenciadorDeBancoDeDados;
import org.shopping.infra.GerenciadorDeDatasource;
import org.shopping.infra.ImplantadorDeAplicacoes;
import org.shopping.infra.MessagesHelper;
import org.shopping.infra.storage.FileStorage;
import org.shopping.services.AplicacaoService;

@Named
@ViewScoped
public class PaginaInicialBean implements Serializable{

	@Inject
	private transient AplicacaoService aplicacaoService;
	
	@Inject
	private transient FileStorage storage;
	
	@Inject
	private transient FacesContext context;

	@Inject
	private transient MessagesHelper mensagens;

	@Inject
	private transient GerenciadorDeBancoDeDados gerenciadorDeBancoDeDados;

	@Inject
	private transient GerenciadorDeDatasource gerenciadorDeDatasource;

	@Inject
	private transient ImplantadorDeAplicacoes implantador;

	private Long aplicacaoId;

	private String senha;

	private boolean autenticado = false;
	
	
	public List<Aplicacao> getAplicacoes() {
		return aplicacaoService.list();
	}

	@Transactional
	public void removerAplicacao() throws IOException {
		Aplicacao aplicacao = aplicacaoService.buscar(aplicacaoId);

		if(aplicacao.getSenha().equals(senha)){
			implantador.undeploy(aplicacao);
			storage.removerPasta(aplicacao.getId().toString());
			aplicacaoService.remover(aplicacao);
            gerenciadorDeBancoDeDados.excluirBancoDeDados(aplicacao.getContexto());

			mensagens.addMessage(new FacesMessage("Aplicação excluída com sucesso!"));
		} else {
			mensagens.addMessage(new FacesMessage("Senha inválida"));
		}

		senha = "";
	}

	public void autenticar() {
		Aplicacao aplicacao = aplicacaoService.buscar(aplicacaoId);

		if(aplicacao.getSenha().equals(senha))
			autenticado = true;
		else
			mensagens.addMessage(new FacesMessage("Senha inválida"));

		senha = "";
	}

	public void atualizarArquivoWar(FileUploadEvent event) throws IOException {

		if(autenticado) {
			Aplicacao aplicacao = aplicacaoService.buscar(aplicacaoId);
			String pasta = aplicacao.getId().toString();
			String nomeDoArquivoWar = aplicacao.getContexto() + ".war";
			String contexto = aplicacao.getContexto();
			String nomeDoDatasource = contexto + "DS";
			String nomeDoBancoDeDados = contexto;

			storage.salvarArquivo(event.getFile().getInputstream(), pasta, nomeDoArquivoWar);
			gerenciadorDeBancoDeDados.criarBancoDeDados(nomeDoBancoDeDados);
			gerenciadorDeDatasource.criar(nomeDoDatasource, nomeDoBancoDeDados);
			implantador.deploy(aplicacao);
			mensagens.addMessage(new FacesMessage("Aplicação atualizada com sucesso!"));
		}

		autenticado = false;
	}

	public Long getAplicacaoId() {
		return aplicacaoId;
	}

	public void setAplicacaoId(Long aplicacaoId) {
		this.aplicacaoId = aplicacaoId;

		autenticado = false;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = DigestUtils.md2Hex(senha);
	}

	public boolean isAutenticado()
	{
		return autenticado;
	}
}
