package org.shopping.managedbeans;

import org.primefaces.model.UploadedFile;
import org.shopping.entidades.jpa.Aplicacao;
import org.shopping.infra.ImplantadorDeAplicacoes;
import org.shopping.infra.MessagesHelper;
import org.shopping.infra.storage.FileStorage;
import org.shopping.services.AplicacaoService;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.PersistenceException;
import javax.transaction.Transactional;

import java.io.IOException;

@Named
@RequestScoped
public class CadastroDeAplicacaoBean {

	@Inject
	private AplicacaoService service;
	
	@Inject
	private FileStorage storage;

	@Inject
	private ImplantadorDeAplicacoes implantador;

	@Inject
	private MessagesHelper mensagens;
	
	private Aplicacao aplicacao;
	private UploadedFile imagem;
	
	@PostConstruct
	public void init() {
		aplicacao = new Aplicacao();
	}
	
	@Transactional
	public String salvar() throws IOException {

        try {
            service.salvar(aplicacao);
            String pasta = aplicacao.getId().toString();
            storage.salvarArquivo(imagem.getInputstream(), pasta, "imagem");

            return "index.xhtml?faces-redirect=true";
        } catch (PersistenceException e) {

            FacesContext.getCurrentInstance().validationFailed();

			if(e.getMessage().contains("ConstraintViolationException")) {
				mensagens.addMessage(new FacesMessage(FacesMessage.SEVERITY_WARN, "Já existe uma aplicação com o mesmo nome de contexto", ""));
			} else {
				mensagens.addMessage(new FacesMessage(FacesMessage.SEVERITY_WARN, "Ocorreu um erro ao salvar a aplicação", ""));
			}

            return "cadastro-de-aplicacao.xhtml";
        }

	}
	
	public Aplicacao getAplicacao() {
		return aplicacao;
	}

	public void setAplicacao(Aplicacao aplicacao) {
		this.aplicacao = aplicacao;
	}

	public UploadedFile getImagem() {
		return imagem;
	}

	public void setImagem(UploadedFile imagem) {
		this.imagem = imagem;
	}	
	
}
