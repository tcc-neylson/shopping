package org.shopping.services;


import java.sql.Connection;
import java.util.List;

import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.validation.ConstraintViolationException;

import org.shopping.entidades.jpa.PersistentEntity;

public abstract class EntityService<T extends PersistentEntity> {

	@PersistenceContext
	protected EntityManager entityManager;
	
	private Class<T> entityClass;
	
	public EntityService(Class<T> entityClass) {
		this.entityClass = entityClass;
	}
	
	public void salvar(T entity) throws ConstraintViolationException {
		entityManager.persist(entity);
		entityManager.flush(); // adicionei para poder capturar as exceções de persistência
	}
	
	public T buscar(Long id) {
		return (T) entityManager.find(entityClass, id);
	}

	public void remover(T entity) {
		entityManager.remove(entity);		
	}

	public T merge(T entity) {
		return entityManager.merge(entity);
	}
	
	public void refresh(T entity) {
		entityManager.refresh(entity);
	}
	
	public List<T> list() {
		CriteriaQuery<T> cq = (CriteriaQuery) entityManager.getCriteriaBuilder().createQuery();
		cq.select(cq.from(entityClass));
		
		return entityManager.createQuery(cq).getResultList();
	}
}
