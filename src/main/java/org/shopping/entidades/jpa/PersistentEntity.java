package org.shopping.entidades.jpa;

public interface PersistentEntity {

	Long getId();
	void setId(Long id);
}
