package org.shopping.entidades.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.apache.commons.codec.digest.DigestUtils;

@Entity
public class Aplicacao implements PersistentEntity {

	
	@Id
	@GeneratedValue
	private Long id;
	private String nome;
	private String senha;

	@Column(unique = true)
	private String contexto;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getContexto() {
		return contexto;
	}

	public void setContexto(String contexto) {
		this.contexto = contexto;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = DigestUtils.md2Hex(senha);
	}
	
	
}
