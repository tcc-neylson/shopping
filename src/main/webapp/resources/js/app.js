function hideModalOnSuccess(xhr, status, args, idModal) {
	
	if (args && !args.validationFailed)
		PF(idModal).hide();
}